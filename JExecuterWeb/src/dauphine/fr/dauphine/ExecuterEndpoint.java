package dauphine.fr.dauphine;



import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.websocket.EncodeException;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.IExecutionEngine;
import fr.dauphine.data.IDataBean;
import fr.dauphine.engine.ExecutionPreferences;
import fr.dauphine.engine.ExecutionResult;
import fr.dauphine.generator.IRandomGenerator;
import fr.dauphine.messages.Message;
import fr.dauphine.messages.Message.Types;
import fr.dauphine.messages.MessageTextDecoder;
import fr.dauphine.messages.MsgBarabasiAlbert;
import fr.dauphine.messages.MsgBarabasiAlbertEncoder;
import fr.dauphine.messages.MsgResult;
import fr.dauphine.messages.MsgUsecase;
import fr.dauphine.messages.ResultEncoder;
import fr.dauphine.service.Service;


@ServerEndpoint(value = "/executerendpoint",
				decoders = { MessageTextDecoder.class },
				encoders = { MsgBarabasiAlbertEncoder.class, ResultEncoder.class }
)
@Stateless
public class ExecuterEndpoint {
	 
	@EJB(beanInterface=IExecutionEngine.class)
	IExecutionEngine executionEngineBean;
	
	@EJB(beanInterface=IRandomGenerator.class)
	IRandomGenerator beanRandomGenerator;
	
	@EJB(beanInterface=IDataBean.class)
	IDataBean dataBean;
	
	private static final Logger logger = Logger.getLogger("ExecuterEndpoint");
	
	
	@OnOpen
	public void openConnection(Session sesion) {
		logger.log(Level.INFO, "open conection");

	}

	@OnMessage
	public void onMessage(Session session, Message msg) {
		logger.log(Level.WARNING, msg.toString()); 

		if(msg instanceof MsgBarabasiAlbert) {
			MsgBarabasiAlbert msgBarabasi = (MsgBarabasiAlbert) msg;
			
			if(msgBarabasi.getGraph() == null) {
				msgBarabasi.setGraph(beanRandomGenerator.generateRandomGraph(msgBarabasi.getVertexCount()));
				msgBarabasi.setType(Types.BARABASI);
				try {
					
					session.getBasicRemote().sendObject(msgBarabasi);
					
				} catch (IOException | EncodeException e) {
					logger.log(Level.WARNING, "onMessage failed", e);
				}
			} else { //execute
				logger.log(Level.INFO, "executing " + msgBarabasi.getExecutionNumber() + " times.");
				for(int i=0; i<msgBarabasi.getExecutionNumber(); i++) {
					logger.log(Level.INFO, "execution request: " + msgBarabasi.getGraph());
					
					ExecutionPreferences preferences = new ExecutionPreferences();
					preferences.setFaultTolerance(msgBarabasi.isFaultTolerance());
					preferences.setSelfhealing(msgBarabasi.isSelfhealing());
					
					ExecutionResult executionResult = executionEngineBean.run(msgBarabasi.getGraph(), preferences);
					
					MsgResult msgResult = new MsgResult();
					msgResult.setType(Types.RESULT);
					msgResult.setSuccess(executionResult.isSuccess());
					
					//save data
					dataBean.save(executionResult, msgBarabasi.getGraph());
					try {
						session.getBasicRemote().sendObject(msgResult);
					} catch (IOException | EncodeException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
				}
				
			}
		} else if(msg instanceof MsgUsecase) {
			Graph<Service, Number> graph = dataBean.readGraph("casestudy/" + ((MsgUsecase)msg).getName() + ".graph");
			//Graph<Service, Number> graph = dataBean.readGraph("casestudy/ehealthappNOFAILURE.graph");
			MsgBarabasiAlbert msgBarabasi = new MsgBarabasiAlbert(graph.getVertexCount());
			msgBarabasi.setGraph(graph);
			msgBarabasi.setType(Types.USECASE);
			try {
				
				session.getBasicRemote().sendObject(msgBarabasi);
				
			} catch (IOException | EncodeException e) {
				logger.log(Level.WARNING, "onMessage failed", e);
			}
		}
	}
	
	
}
