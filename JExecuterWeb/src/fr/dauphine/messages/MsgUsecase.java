package fr.dauphine.messages;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.service.Service;

public class MsgUsecase extends Message {

	private String name;
	private Graph<Service, Number> graph;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Graph<Service, Number> getGraph() {
		return graph;
	}

	public void setGraph(Graph<Service, Number> graph) {
		this.graph = graph;
	}

}
