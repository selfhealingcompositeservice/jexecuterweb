package fr.dauphine.messages;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.graphanalyser.GraphAnalyser;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.graphutils.JSONUtils;
import fr.dauphine.service.Data;
import fr.dauphine.service.Service;

public class MsgBarabasiAlbertEncoder implements Encoder.Text<MsgBarabasiAlbert>{

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(EndpointConfig arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public String encode(MsgBarabasiAlbert msg) throws EncodeException {
		JsonObjectBuilder builder = Json.createObjectBuilder();
		builder.add("type", msg.getType().toString());
		builder.add("vertexCount", new Integer(msg.getGraph().getVertexCount()).toString());

		builder.add("graph", JSONUtils.graphToJson(msg.getGraph()));
		
		//Graph information
		Graph<Service, Number> graph = msg.getGraph();
		GraphUtils.addControlNodes(graph);
		GraphAnalyser analyser  = new GraphAnalyser(graph);
		builder.add("tp", analyser.getTransactionalProperty().toString());
		builder.add("time", analyser.getEstimatedExecutionTime());
		builder.add("price", analyser.getEstimatedPrice());
		builder.add("reputation", analyser.getEstimatedReputation());
		builder.add("availability", analyser.getEstimatedAvailability());
		GraphUtils.removeControlNodes(graph);

		//build json array of graph inputs
		JsonArrayBuilder graphInputBuilder = Json.createArrayBuilder();
		for(Data i:GraphUtils.getInputs(msg.getGraph())) {
			JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
			jsonBuilder.add("name", i.getName());
			graphInputBuilder.add(jsonBuilder);
		}
		builder.add("inputs", graphInputBuilder.build());

		//build json array of graph outputs
		JsonArrayBuilder graphOutputBuilder = Json.createArrayBuilder();
		for(Data i:GraphUtils.getOutputs(msg.getGraph())) {
			JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
			jsonBuilder.add("name", i.getName());
			graphOutputBuilder.add(jsonBuilder);
		}
		builder.add("outputs", graphOutputBuilder.build());

		String jsonText = builder.build().toString();


		return jsonText;
	}


}
