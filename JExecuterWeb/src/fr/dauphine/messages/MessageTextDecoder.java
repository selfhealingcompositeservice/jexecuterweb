package fr.dauphine.messages;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonObject;
import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.graphutils.JSONUtils;
import fr.dauphine.messages.Message.Types;
import fr.dauphine.service.Service;

@Stateless
public class MessageTextDecoder implements Decoder.Text<Message> {
	
	private static final Logger logger = Logger.getLogger("ExecuterEndpoint");

	/* Stores the name-value pairs from a JSON message as a Map */
	private Map<String, Object> messageMap;

	private Map<String, Object> graphMap;

	@Override
	public Message decode(String string) throws DecodeException {
		Message msg = null;
		Graph<Service, Number> graph = null;
		if (willDecode(string)) {
			switch ((String) messageMap.get("type")) {
			case "barabasialbert":
				MsgBarabasiAlbert msgBarabasi = new MsgBarabasiAlbert(
						new Integer((String) messageMap.get("vertexCount")));

				
				graph = JSONUtils.mapToGraph(graphMap);
				if (graph != null) {
					msgBarabasi.setGraph(graph);
					msgBarabasi.setExecutionNumber((int) Integer.parseInt((String) messageMap.get("executionNumber")));
					msgBarabasi.setFaultTolerance(Boolean.parseBoolean((String) messageMap.get("faultTolerance")));
					msgBarabasi.setSelfhealing(Boolean.parseBoolean((String) messageMap.get("selfHealing")));
				}
				msg = msgBarabasi;
				break;
			case "casestudy":
				MsgUsecase msgUsecase = new MsgUsecase();
				msgUsecase.setName((String) messageMap.get("name"));
				msg = msgUsecase;
				break;
			}
		} else {
			throw new DecodeException(string, "[Message] Can't decode.");
		}
		return msg;
	}

	@Override
	public boolean willDecode(String string) {
		messageMap = new HashMap<String, Object>();

		logger.log(Level.INFO, "willDecode" + string);

		JsonObject obj = Json.createReader(new StringReader(string))
				.readObject();

		messageMap.put("type", obj.getString("type"));
		messageMap.put("vertexCount", obj.getString("vertexCount"));
		
		if(messageMap.get("type").equals(Types.USECASE.toString()))
			messageMap.put("name", obj.getString("name"));
		
		if(obj.containsKey("executionNumber"))
			messageMap.put("executionNumber", obj.getString("executionNumber"));
		if(obj.containsKey("faultTolerance"))
			messageMap.put("faultTolerance", obj.getString("faultTolerance"));
		if(obj.containsKey("selfHealing"))
			messageMap.put("selfHealing", obj.getString("selfHealing"));
	
		graphMap = JSONUtils.stringToMap(string);
		logger.log(Level.INFO, "AQUI " + graphMap);
		logger.log(Level.INFO, "willDecode MAP" + messageMap);
		return true;
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(EndpointConfig arg0) {
		// TODO Auto-generated method stub

	}
}