package fr.dauphine.messages;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

public class ResultEncoder implements Encoder.Text<MsgResult> {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(EndpointConfig config) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String encode(MsgResult msg) throws EncodeException {
		JsonObjectBuilder builder = Json.createObjectBuilder();
		builder.add("type", msg.getType().toString());
		builder.add("success", msg.isSuccess());
		
		return builder.build().toString();
	}

}
