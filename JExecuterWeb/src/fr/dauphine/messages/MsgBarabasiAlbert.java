package fr.dauphine.messages;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.service.Service;

public class MsgBarabasiAlbert extends Message {
	
	private int vertexCount;
	private Graph<Service, Number> graph;
	private int executionNumber;
	private boolean isFaultTolerance;
	private boolean isSelfhealing;

	public MsgBarabasiAlbert(int vertexCount) {
		super();
		this.vertexCount = vertexCount;
	}

	public int getVertexCount() {
		return vertexCount;
	}

	public void setVertexCount(int vertexCount) {
		this.vertexCount = vertexCount;
	}

	public Graph<Service, Number> getGraph() {
		return graph;
	}

	public void setGraph(Graph<Service, Number> graph) {
		this.graph = graph;
	}

	@Override
	public String toString() {
		return "MsgBarabasiAlbert [vertexCount=" + vertexCount + ", graph="
				+ graph + "]";
	}

	public int getExecutionNumber() {
		return executionNumber;
	}

	public void setExecutionNumber(int executionNumber) {
		this.executionNumber = executionNumber;
	}

	public boolean isFaultTolerance() {
		return isFaultTolerance;
	}

	public void setFaultTolerance(boolean isFaultTolerance) {
		this.isFaultTolerance = isFaultTolerance;
	}

	public void setSelfhealing(boolean isSelfhealing) {
		this.isSelfhealing = isSelfhealing;
	}
	
	public boolean isSelfhealing() {
		return this.isSelfhealing;
	}
	
	

}
