package fr.dauphine.messages;

public class Message {
	
	public enum Types {
	    BARABASI("barabasi"),
	    RESULT("result"),
	    USECASE("casestudy"),
	    ;

	    private final String type;

	    /**
	     * @param text
	     */
	    private Types(final String type) {
	        this.type = type;
	    }

	    /* (non-Javadoc)
	     * @see java.lang.Enum#toString()
	     */
	    @Override
	    public String toString() {
	        return type;
	    }
	}
	
	private Types type;

	public Message(Types type) {
		super();
		this.type = type;
	}

	public Message() {
		// TODO Auto-generated constructor stub
	}

	public Types getType() {
		return type;
	}

	public void setType(Types type) {
		this.type = type;
	}

}
