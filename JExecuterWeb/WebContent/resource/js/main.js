//http://jsfiddle.net

//example: http://jsfiddle.net/hfg3r8k7/

var wsocket;
var serviceLocation = "ws://localhost:8080/JExecuterWeb/executerendpoint";
var renderer;
var layouter;
var g;
var services = {};
var spinner;
var spinnerCanvas;
var opts = {
		lines: 13, // The number of lines to draw
		length: 70, // The length of each line
		width: 10, // The line thickness
		radius: 100, // The radius of the inner circle
		corners: 1, // Corner roundness (0..1)
		rotate: 0, // The rotation offset
		direction: 1, // 1: clockwise, -1: counterclockwise
		color: '#000', // #rgb or #rrggbb or array of colors
		speed: 1, // Rounds per second
		trail: 60, // Afterglow percentage
		shadow: false, // Whether to render a shadow
		hwaccel: false, // Whether to use hardware acceleration
		className: 'spinner', // The CSS class to assign to the spinner
		zIndex: 2e9, // The z-index (defaults to 2000000000)
		top: '50%', // Top position relative to parent
		left: '50%' // Left position relative to parent
};

function Service(name, tp, time, price, reputation, availability, inputs, outputs) {
	this.name = name;
	this.tp = tp;
	this.time = time;
	this.price = price;
	this.reputation = reputation;
	this.availability = availability;
	this.inputs = inputs;
	this.outputs = outputs;
}

/* Example Graph
{"vertexCount":"2","vertices":[{"name":"1"},{"name":"0"}],"edges":[{"first":"1","second":"0"}]}
 */
function onMessageReceived(evt) {


	//alert(evt.data);
	var msg = JSON.parse(evt.data); //native API

	if(msg.type == "result")
		showResult(msg);
	else if(msg.type == "barabasi" || msg.type == "casestudy")
		createGraph(msg);
	else
		console.error("unknown received message type");
}

function showResult(msg) {
	spinner.stop();
	spinnerCanvas.stop();
	$("#resultSuccess").val(msg.success);
}

function createGraph(msg) {
	$( "#canvas" ).empty();

	g = new Graph();

	//alert(msg.vertexCount);
	jsonGraph = msg.graph;
	vertices = jsonGraph.vertices;
	console.log("onMessageReceived: " + JSON.stringify(jsonGraph));


	for(var i=0;i<vertices.length;i++){
		var v = vertices[i];

		var inputs = "i={" + joinNames(v.inputs) + "}";
		var outputs = "i={" + joinNames(v.outputs) + "}";

		g.addNode(v.name , { label : v.name + " " + v.tp + " " + inputs + " " + outputs});

		//store the service object
		var service = new Service(v.name, v.tp, v.time, v.price, v.reputation, v.availability, v.inputs, v.outputs);

		services[v.name] = service;
	}



	edges = jsonGraph.edges;

	for(var i=0;i<edges.length;i++){
		var obj = edges[i];
		//alert(JSON.stringify(obj));
		//alert(JSON.stringify(obj.first));
		g.addEdge(obj.first, obj.second, { directed : true } );
	}



	/* layout the graph using the Spring layout implementation */
	layouter = new Graph.Layout.Spring(g);
	var width = $(document).width() - 20;
	var height = $(document).height() - 60;
	/* draw the graph using the RaphaelJS draw implementation */
	renderer = new Graph.Renderer.Raphael('canvas', g, width, height);

	layouter.layout();
	renderer.draw();

	//console.log(g.nodes);
	/*for(var i in g.nodes){

		   g.nodes[i].shape.click(function(event){
			   alert(event.target);
		    console.log(event);
		    console.log(event.target.getAttribute("id"));

		});
	}*/

	$("#tp").val(msg.tp);
	$("#eet").val(msg.time);
	$("#eep").val(msg.price);
	$("#eer").val(msg.reputation);
	$("#eea").val(msg.availability);
	$("#inputs").val(joinNames(msg.inputs));
	$("#outputs").val(joinNames(msg.outputs));
	showExecutionMenu();
	$('#generateBarabasiMenu').hide();
}

function showExecutionMenu() {
	$('#executeMenu').show();
	$('#executeResult').show();
}

function hideExecutionMenu() {
	$('#executeMenu').hide();
	$('#executeResult').hide();
}

function joinNames(array) {
	var value = "";
	for(var i=0; i<array.length; i++){
		value += array[i].name;
		if(i < array.length-1)
			value += ", ";
	}

	return value;
}

function create() {
	var msg = '{"type": ' + '"barabasialbert"' + ',' + 
	'"vertexCount": "' + $vertexCount.val() + '"'


	+ '}';

	wsocket.send(msg);

}

function getCaseStudy(name) {
	var msg = '{"type": ' + '"casestudy"' + ',' + 
	'"vertexCount": "' + 0 + '"' + ',' + 
	'"name": "' + name + '"'
	+ '}';

	wsocket.send(msg);
}


/* Example Graph
{"vertexCount":"2","vertices":[{"name":"1"},{"name":"0"}],"edges":[{"first":"1","second":"0"}]}
 */
function execute() {
	
	var array = $.map(g.nodes, function(value, index) {
		return [value];
	});

	var msg = '{"type": ' + '"barabasialbert"' + ',' + 
	'"vertexCount": "' + array.length + '"' + ',' + 
	'"executionNumber": "' + $executionNumber.val() + '"' + ',' + 
	'"faultTolerance": "' + $('#isfaulttolerance').is(':checked') + '"' + ',' + 
	'"selfHealing": "' + $('#isSelfhealing').is(':checked') + '"' + ',' + 
	'"vertices":[';

	

	for(var i=0; i<array.length; i++) {
		msg += JSON.stringify(services[array[i].id]);
		if(i < array.length - 1)
			msg += ",";
	}

	msg += '],';

	//edges

	msg += '"edges":[';

	/*array = $.map(g.edges, function(value, index) {
	    return [value];
	});*/

	for(var i=0;i<g.edges.length;i++){
		msg += '{"first":"' + g.edges[i].source.id 
		+ '","second":"' + g.edges[i].target.id + '"}';

		if(i < g.edges.length - 1)
			msg += ',';
	}

	msg += ']';


	msg += '}';

	console.log("Sending to execute: " + msg);



	//alert(JSON.stringify(g));

	wsocket.send(msg);

	var target = document.getElementById('executionMenu');
	spinner = new Spinner(opts).spin(target);
	var target2 = document.getElementById('canvas');
	spinnerCanvas = new Spinner(opts).spin(target2);
}

function showBarabasi() {
	$('#generateBarabasiMenu').show();
	$('#mainMenu').hide();
}

function showManual() {
	$('#generateBarabasiMenu').hide();
	$('#mainMenu').hide();
	$('#manualMenu').show();
}

function showCaseStudy() {
	$('#mainMenu').hide();
	$('#caseStudyMenu').show();
}


function addNode() {
	alert(newEdgeName.value);

	dialog.dialog( "close" );
}

var dialog, form;
$(document).ready(function() {


	$vertexCount = $('#vertexCount');
	$executionNumber = $('#executionNumber');

	wsocket = new WebSocket(serviceLocation);
	wsocket.onmessage = onMessageReceived;

	$('#generateBarabasiMenu').hide();
	hideExecutionMenu();
	$('#manualMenu').hide();
	
	$('#caseStudyMenu').hide();
	
	//go directly to case study
	$('#mainMenu').hide();
	console.log('before');
	setTimeout(function(){
		getCaseStudy('ehealthapp');
	},500);
	
	//end go directly to case study

	$('#do-create').submit(function(evt) {
		evt.preventDefault();
		create();
	});

	$('#do-execute').submit(function(evt) {
		evt.preventDefault();
		execute();
	});

	$('#barabasi').click(function(){
		showBarabasi();
	});

	$('#manual').click(function(){
		showManual();
	});
	
	$('#caseStudy').click(function(){
		showCaseStudy();
	});
	
	$('#ehealthapp').click(function(){
		getCaseStudy('ehealthapp');
	});

	dialog = $( "#dialog-form" ).dialog({
		autoOpen: false,
		height: 300,
		width: 350,
		modal: true,
		buttons: {
			"Create an account": addNode,
			Cancel: function() {
				dialog.dialog( "close" );
			}
		},
		close: function() {
			form[ 0 ].reset();
			allFields.removeClass( "ui-state-error" );
		}
	});

	form = dialog.find( "form" ).on( "submit", function( event ) {
		event.preventDefault();
		alert("new node");
	});

	$( "#newNode" ).button().on( "click", function() {
		dialog.dialog( "open" );
	});

});