<html>
<head>
    <script type="text/javascript" src="./resource/js/dracula/js/raphael-min.js"></script>
    <script type="text/javascript" src="./resource/js/dracula/js/dracula_graffle.js"></script>
    <script type="text/javascript" src="./resource/js/dracula/js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="./resource/js/dracula/js/dracula_graph.js"></script>
    <script type="text/javascript" src="./resource/js/jquery.leanModal.min.js"></script>
    <script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./resource/js/main.js"></script>
    <script type="text/javascript" src="./resource/js/spin.js"></script>
    <script src="http://code.jquery.com/ui/1.11.3/jquery-ui.js"></script>


    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="./resource/css/style.css">
</head>




<body>

<h2 id="pagetitle">Transactional Composite Web Service Execution</h2>

<div id="canvas" style="position: relative"></div>


<!-- Main Menu -->
<div id="mainMenu">
<form action="" method="post" class="elegant-aero">
    <h1>Graph options:
    </h1>
    <label>
        <input id="barabasi" class="button" value="Random Barabasi-Albert" />
    </label>  
   
     <label>
        <input  id="manual" class="button" value="Create Graph Manually" />
    </label> 
    <label>
        <input  id="caseStudy" class="button" value="Case Study" />
    </label>     
</form>
</div>

<!--  Case Study -->
<div id="caseStudyMenu">
<form action="" method="post" class="elegant-aero">
    <h1>Case study:
    </h1>
    <label>
        <input id="ehealthapp" class="button" value="e-Health Application" />
    </label>    
</form>
</div>

<!-- Barabasi Albert Generation Menu -->
<div id="generateBarabasiMenu">
 <span>&nbsp;</span>
  <form id="do-create" action="" method="post" class="elegant-aero">
    <h1>Random Barabasi-Albert CWS
    </h1>
    <label>
        <span>Number of vertices :</span>
        <input id="vertexCount" type="text" name="name" placeholder="Number of vertices" />
    </label>
   
    <!--  <label>
        <span>Your Email :</span>
        <input id="email" type="email" name="email" placeholder="Valid Email Address" />
    </label>
   
    <label>
        <span>Message :</span>
        <textarea id="message" name="message" placeholder="Your Message to Us"></textarea>
    </label>
     <label>
        <span>Subject :</span><select name="selection">
        <option value="Job Inquiry">Job Inquiry</option>
        <option value="General Question">General Question</option>
        </select>
    </label>   --> 
     <label>
        <span>&nbsp;</span>
        <input type="submit" class="button" value="Create" />
    </label>    
</form>
</div>


<div id="executionMenu" style="overflow: hidden; position: relative;">
<!-- Execution Menu -->
<div id="executeMenu" style="width: 600px; float: left;">
<form id="do-execute" action="" method="post" class="elegant-aero">
    <h1>Execution:
    </h1>
    <label>
    	<span>Time:</span>
    	<input id="eet" type="text" value="N/A" class="field left" readonly>
    </label>
    <label>
    	<span>Price:</span>
    	<input id="eep" type="text" value="N/A" class="field left" readonly>
    </label>
    <label>
    	<span>Reputation:</span>
    	<input id="eer" type="text" value="N/A" class="field left" readonly>
    </label>
    <label>
    	<span>Availability:</span>
    	<input id="eea" type="text" value="N/A" class="field left" readonly>
    </label>
    <label>
    	<span>Inputs:</span>
    	<input id="inputs" type="text" value="N/A" class="field left" readonly>
    </label>
    <label>
    	<span>Outputs:</span>
    	<input id="outputs" type="text" value="N/A" class="field left" readonly>
    </label>
    <label>
    	<span>Transactionality:</span>
    	<input id="tp" type="text" value="N/A" class="field left" readonly>
    </label>
       <label>
        <span>Number of executions:</span>
        <input id="executionNumber" type="text" name="executionNumber" placeholder="Number of executions" />
    </label>
    <label>
        <span>Fault tolerance:</span>
    	<input type="checkbox" name="isfaulttolerance" id="isfaulttolerance" value="Fault tolerance" checked><br>
	</label>    
	<label>
        <span>Self-healing:</span>
    	<input type="checkbox" name="isSelfhealing" id="isSelfhealing" value="Self-healing" checked><br>
	</label> 
     <label>
     	<span>&nbsp;</span>
        <input type="submit" class="button" value="Execute" />
    </label>    
</form>
</div>
<!-- Execution Result -->
<div id="executeResult" style="margin-left: 100px;">
<form id="do-execute-result" action="" method="post" class="elegant-aero">
    <h1>Execution Result:
    </h1>
    <label>
    	<span>Success:</span>
    	<input id="resultSuccess" type="text" value="N/A" class="field left" readonly>
    </label>
    <label>
    	<span>Time:</span>
    	<input id="eet" type="text" value="N/A" class="field left" readonly>
    </label>
    <label>
    	<span>Price:</span>
    	<input id="eep" type="text" value="N/A" class="field left" readonly>
    </label>
    <label>
    	<span>Reputation:</span>
    	<input id="eer" type="text" value="N/A" class="field left" readonly>
    </label>
    <label>
    	<span>Availability:</span>
    	<input id="eea" type="text" value="N/A" class="field left" readonly>
    </label>
    <label>
    	<span>Inputs:</span>
    	<input id="inputs" type="text" value="N/A" class="field left" readonly>
    </label>
    <label>
    	<span>Outputs:</span>
    	<input id="outputs" type="text" value="N/A" class="field left" readonly>
    </label>
    <label>
    	<span>Transactionality:</span>
    	<input id="tp" type="text" value="N/A" class="field left" readonly>
    </label>
     <label>
     	<span>&nbsp;</span>
        <input type="submit" class="button" value="Execute" />
    </label>    
</form>
</div>
</div>

<!-- Manual Execution Menu -->
<div id="manualMenu"> 
<form action="" method="post" class="elegant-aero">
    <h1>Execution:
    </h1>
    <label>
        <input id="newNode" class="button"  data-reveal-id="myModal" value="New Node" />
    </label>  
     <label>
        <input id="newEdge" class="button" value="New Edge" />
    </label>    
</form>
</div>

<!-- Modal Form -->
<div id="dialog-form" title="Create new user">
	<p class="validateTips">All form fields are required.</p>
	<form>
		<label>
	        <input id="newEdgeName" class="button" value="New Edge Name" />
	    </label>    
	    <label>
	        <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
	    </label>    
	</form>
</div>

</body>

</html>
